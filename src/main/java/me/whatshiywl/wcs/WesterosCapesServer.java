package me.whatshiywl.wcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class WesterosCapesServer extends JavaPlugin {

	public static Logger log;
	public static GMHook gmhook;
	public static HashMap<Player, String> capedplayers = new HashMap<Player, String>();
	public static List<String> capes = new ArrayList<String>();
	public static boolean groupmanager;
	public static final String LABEL = ChatColor.AQUA
			+ "[" + ChatColor.GOLD + "WesterosCapesServer" + ChatColor.AQUA + "] ";

	@Override
	public void onEnable() {

		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "WesterosCapes");
		saveDefaultConfig();
		log = this.getLogger();
		if(this.setupGroupManager()) {
			gmhook = new GMHook(this);
			gmhook.onPluginEnable();
			log.info("GroupManager found and hooked!");
		} else {
			log.info("[WARNING] Could not find GroupManager! The plugin will not function correctly!");
		}
		loadCapes();
		getServer().getPluginManager().registerEvents(new PluginListener(this), this);
	}

	@Override
	public void onDisable() {
		log = null;
		gmhook = null;
		capedplayers.clear();
		groupmanager = false;
	}


	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!(sender instanceof Player))return true;
		if((label.equalsIgnoreCase("westeroscapes") || label.equalsIgnoreCase("wc"))){
			if(args.length > 0){
				if(args[0].equalsIgnoreCase("list")){
					if(!sender.hasPermission("westeroscapes.list")){
						sender.sendMessage(ChatColor.RED + "You do not have permissions for this.");
						return true;
					}
					boolean showurl = false;
					if(args.length > 1) showurl = args[1].equalsIgnoreCase("-url");
					String msg = ChatColor.GOLD + "Existing cape ID's:\n";
					for(int i=0; i < capes.size(); i++){
						String cape = capes.get(i);
						String id = showurl ? cape : cape.split(";")[0];
						msg += ChatColor.BLUE + id;
						if(i < capes.size()-1) msg += ", ";
					}
					sender.sendMessage(msg);
					return true;
				}

				if(args[0].equalsIgnoreCase("set")){
					System.out.println(getConfig().getBoolean("allowselfcape"));
					if(!getConfig().getBoolean("allowselfcape") && !gmhook.hasPermission((Player) sender, "westeroscapes.set")){
						sender.sendMessage(ChatColor.RED + "You do not have permissions for this.");
						return true;
					}
					String id = args[1];
					if(!gmhook.hasPermission((Player) sender, "westeroscapes.use." + id.toLowerCase())){
						sender.sendMessage(ChatColor.RED + "You do not have permissions for this cape.");
						return true;
					}
					gmhook.SetPlayerCape((Player) sender, id);
					updatePlayer((Player) sender, 0);

					return true;
				}

				//Must be last command!
				if(Bukkit.getServer().getPlayer(args[0]) != null){
					if(!sender.hasPermission("westeroscapes.lookup")){
						sender.sendMessage(ChatColor.RED + "You do not have permissions for this.");
						return true;
					}
					Player player = getServer().getPlayer(args[0]);
					if(!capedplayers.containsKey(player)){
						sender.sendMessage(ChatColor.RED + "This player does not have a cape.");
						return true;
					}
					String id = capedplayers.get(player);
					boolean showurl = false;
					if(args.length > 1) showurl = args[1].equalsIgnoreCase("-url");
					String msg = id;
					if(showurl){
						for(String cape : capes){
							String capeid = cape.split(";")[0];
							if(capeid.equals(id)){
								msg = cape;
								break;
							}
						}
					}
					sender.sendMessage(ChatColor.GOLD + args[0] + "'s cape is " + ChatColor.BLUE + msg + ChatColor.GOLD + "!");
					return true;
				}
			}
			sender.sendMessage(ChatColor.RED + "This command does not exist. Type /help westeroscapes for help.");
		}
		return true;
	}

	private boolean setupGroupManager() {
		log.info("Looking for GroupManager...");
		groupmanager = (getServer().getPluginManager().getPlugin("GroupManager") != null);
		return groupmanager;
	}

	private void loadCapes(){
		if(!getDataFolder().exists())getDataFolder().mkdir();
		File capesFile = new File(getDataFolder(), "capes.yml");
		try {
			if(capesFile.createNewFile()){
				BufferedWriter writer = null;
				try {
					writer = new BufferedWriter(new OutputStreamWriter(
							new FileOutputStream(capesFile), "utf-8"));
					writer.write("#format: ID;URL\n" +
							"Smojang;http://i.imgur.com/TiCC54H.png\n" +
							"Sawesome;http://i.imgur.com/7WbVVDJ.png");
					writer.flush();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {writer.close();} catch (Exception ex) {}
				}
			}

			BufferedReader reader = null;
			reader = new BufferedReader(new FileReader(capesFile));
			String line;
			while ((line = reader.readLine()) != null) {
				if(line.startsWith("#"))continue;
				capes.add(line);
			}
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void updatePlayer(final Player player, long delay) {
        if(!WesterosCapesServer.groupmanager)return;
		//Send player information to other players
		GMHook gmhook = WesterosCapesServer.gmhook;
		final String id = gmhook.getPlayerCape(player);
		//WesterosCapesServer.log.info("id(" + player.getName() + ")=" + id);
		if ((id != null) && (id.length() > 0)) {
			if(!gmhook.hasPermission(player, "westeroscapes.use." + id.toLowerCase())){
				WesterosCapesServer.log.info(player.getName() + " does not have permissions for the " + id + " cape");
				return;
			}
			WesterosCapesServer.capedplayers.put(player, id);
			final WesterosCapesServer plugin = this;
			getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					//WesterosCapesServer.log.info("Sending cape " + id + " to " + player.getName());
					String message = "ADD;" + player.getName() + ";" + id;
					for(Player onlineplayer : getServer().getOnlinePlayers()){
						onlineplayer.sendPluginMessage(plugin, "WesterosCapes", message.getBytes());
					}
				}
			}, delay);
		}
	}

}