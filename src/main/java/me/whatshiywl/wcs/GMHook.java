package me.whatshiywl.wcs;

import java.util.Arrays;
import java.util.List;
import org.anjocaido.groupmanager.GroupManager;
import org.anjocaido.groupmanager.data.Group;
import org.anjocaido.groupmanager.data.User;
import org.anjocaido.groupmanager.dataholder.OverloadedWorldHolder;
import org.anjocaido.groupmanager.permissions.AnjoPermissionsHandler;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

public class GMHook{
	private GroupManager groupmanager;
	private WesterosCapesServer plugin;

	public GMHook(final WesterosCapesServer plugin){
		this.plugin = plugin;
	}

	public void onPluginEnable(){
		final PluginManager pluginManager = plugin.getServer().getPluginManager();
		final Plugin GMplugin = pluginManager.getPlugin("GroupManager");
		if (GMplugin != null && GMplugin.isEnabled()){
			groupmanager = (GroupManager)GMplugin;
		}
	}

	public String getGroup(final Player base){
		final AnjoPermissionsHandler handler = groupmanager.getWorldsHolder().getWorldPermissions(base);
		if (handler == null){
			return null;
		}
		return handler.getGroup(base.getName());
	}

	public boolean setGroup(final Player base, final String group){
		final OverloadedWorldHolder handler = groupmanager.getWorldsHolder().getWorldData(base);
		if (handler == null){
			return false;
		}
		handler.getUser(base.getName()).setGroup(handler.getGroup(group));
		return true;
	}

	public List<String> getGroups(final Player base){
		final AnjoPermissionsHandler handler = groupmanager.getWorldsHolder().getWorldPermissions(base);
		if (handler == null){
			return null;
		}
		return Arrays.asList(handler.getGroups(base.getName()));
	}

	public String getPrefix(final Player base){
		final AnjoPermissionsHandler handler = groupmanager.getWorldsHolder().getWorldPermissions(base);
		if (handler == null){
			return null;
		}
		return handler.getUserPrefix(base.getName());
	}

	public String getSuffix(final Player base){
		final AnjoPermissionsHandler handler = groupmanager.getWorldsHolder().getWorldPermissions(base);
		if (handler == null){
			return null;
		}
		return handler.getUserSuffix(base.getName());
	}

	public boolean hasPermission(final Player base, final String node){
	    OverloadedWorldHolder owh = groupmanager.getWorldsHolder().getDefaultWorld();
	    if (owh != null){
	    	User user = owh.getUser(base.getName());
	    	if (user != null){
	    		if(user.getPermissionList().contains(node)) return true;
	    	}
	    }
	    final AnjoPermissionsHandler handler = groupmanager.getWorldsHolder().getWorldPermissions(base);
		if (handler == null){
			return false;
		}
		return handler.has(base, node);
	}

	/**
	 * Returns the cape ID set for that player, if any. Priority goes to player variables and then group variables.
	 * @param player The player
	 * @return The cape ID (ie. Smojang) for that player or null if none is set
	 */
	public String getPlayerCape(Player player){
		return getVariableByPlayer(player, "cape");
	}

	public String getVariableByPlayer(Player player, String var) {
	    OverloadedWorldHolder owh = groupmanager.getWorldsHolder().getDefaultWorld();
	    if (owh == null) return null;
	    User user = owh.getUser(player.getName());
	    if (user == null) return null;
	    if (user.getVariables().hasVar(var) && user.getVariables().getVarString(var) != null){
	    	return user.getVariables().getVarString(var);
	    }
	    Group grp = owh.getGroup(this.getGroup(player));
	    if (grp == null) return null;
	    return grp.getVariables().getVarString(var);
	}

	public void SetPlayerCape(Player player, String cape){
		setVariableByPlayer(player, "cape", cape);
	}

	public void setVariableByPlayer(Player player, String var, String value){
	    OverloadedWorldHolder owh = groupmanager.getWorldsHolder().getDefaultWorld();
	    if (owh == null) return;
	    User user = owh.getUser(player.getName());
	    if (user == null) return;
		if (user.getVariables().hasVar(var)) user.getVariables().removeVar(var);
		user.getVariables().addVar("cape", value);
	}
}