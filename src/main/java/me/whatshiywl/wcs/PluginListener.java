package me.whatshiywl.wcs;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PluginListener implements Listener{

	private WesterosCapesServer plugin;

	public PluginListener(final WesterosCapesServer plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent event){
		if(!WesterosCapesServer.groupmanager)return;
		final Player player = event.getPlayer();

		//Send all cape information to the player
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				//WesterosCapesServer.log.info("Sending cape defs to " + player.getName());
				for(String capeInfo : WesterosCapesServer.capes){
					player.sendPluginMessage(plugin, "WesterosCapes", capeInfo.getBytes());
				}
				// And send capes for other players
				for (Player p : WesterosCapesServer.capedplayers.keySet()) {
				    if (p != player) continue;
                    String message = "ADD;" + p.getName() + ";" + WesterosCapesServer.capedplayers.get(p);
                    player.sendPluginMessage(plugin, "WesterosCapes", message.getBytes());
				}
			}
		}, 50);

		plugin.updatePlayer(player, 100);

        //Send player capes for existing players
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public void run() {
                // And send capes for other players
                for (Player p : WesterosCapesServer.capedplayers.keySet()) {
                    if (p == player) continue;
                    String message = "ADD;" + p.getName() + ";" + WesterosCapesServer.capedplayers.get(p);
                    player.sendPluginMessage(plugin, "WesterosCapes", message.getBytes());
                }
            }
        }, 100);
	}

	@EventHandler
	public void onPlayerQuitEvent(PlayerQuitEvent event){
		if(!WesterosCapesServer.groupmanager)return;
		Player player = event.getPlayer();
		WesterosCapesServer.capedplayers.remove(player);
		String message = "REMOVE;" + player.getName();
		for(Player p : plugin.getServer().getOnlinePlayers()){
			p.sendPluginMessage(plugin, "WesterosCapes", message.getBytes());
		}
	}

	@EventHandler
	public void onPlayerChangedWorldEvent(PlayerChangedWorldEvent event){
		if(!WesterosCapesServer.groupmanager)return;
		final Player player = event.getPlayer();
		WesterosCapesServer.capedplayers.remove(player);
		String message = "REMOVE;" + player.getName();
		for(Player onlineplayer : plugin.getServer().getOnlinePlayers()){
			onlineplayer.sendPluginMessage(plugin, "WesterosCapes", message.getBytes());
		}

		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				plugin.updatePlayer(player, 1);
			}
		}, 1L);
	}

	@EventHandler
	public void onPlayerRespawnEvent(PlayerRespawnEvent event){
		if(!WesterosCapesServer.groupmanager)return;
		final Player player = event.getPlayer();
		WesterosCapesServer.capedplayers.remove(player);
		String message = "REMOVE;" + player.getName();
		for(Player onlineplayer : plugin.getServer().getOnlinePlayers()){
			onlineplayer.sendPluginMessage(plugin, "WesterosCapes", message.getBytes());
		}

		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				plugin.updatePlayer(player, 1);
			}
		}, 1L);
	}
}